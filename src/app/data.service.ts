import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  email:string;
  password:string;
  phone:number;
  birthday:string;
}

@Injectable({
  providedIn: 'root'
})

export class DataService {

  _choosePlan:string;
  _numOfMonths:number;
  _price:number;

  /**************************************** */


              set choosePlan(value: string) {
                this._choosePlan = value;
              }
              get choosePlan(): string {
                return this._choosePlan;
              }
 
/**************************************** */

                set numOfMonths(value: number) {
                  this._numOfMonths = value;
                }

                get numOfMonths(): number {
                  return this._numOfMonths;
                }
/**************************************** */
                set price(value: number) {
                  this._price = value;
                }

                get price(): number {
                  return this._price;
                }


  constructor( private http:HttpClient) { }

  getData(url){
    return this.http.get('http://localhost/gift/get.php');
  }
}