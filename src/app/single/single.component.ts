import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router} from '@angular/router';
import { DataService } from '../data.service';


import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.css']
})
export class SingleComponent implements OnInit {
  
  choosePlan:string;
  price:number;

  cookieValue = '';

  constructor( private cookieService: CookieService,private dataService: DataService,private router: Router, private location: Location) { }

  cancel() {
    this.choosePlan = '';
    this.location.back(); // <-- go back to previous location on cancel
  }

  close(){
    this.router.navigate(['./']);
  }

  ngOnInit() {
    this.cookieValue = this.cookieService.get('checkEmail');
    this.choosePlan = this.dataService.choosePlan;
  }

  onClickRegister(price){
    if(this.cookieValue){
      this.router.navigate(['./gift/dashboard']);
    }else{
      this.dataService.choosePlan = this.dataService.choosePlan
      this.dataService.numOfMonths = null;
      this.dataService.price = price;
      this.router.navigate(['/gift/register']);
    }
    

  }


}
