import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { DataService } from '../data.service';

@Component({
  selector: 'app-gift',
  templateUrl: './gift.component.html',
  styleUrls: ['./gift.component.css']
})
export class GiftComponent implements OnInit {

  title = 'Gift Page'
 choosePlan:string;
 
  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
  }

  onClickSingle() {
    this.dataService.choosePlan = 'Single';
    this.dataService.numOfMonths = null;
    this.router.navigate(['/gift/single']);
  }

  onClickMonthly() {
    this.dataService.choosePlan = 'Monthly';
    this.dataService.numOfMonths = null;
    this.router.navigate(['/gift/monthly']);
  }

}
