import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import {ActivatedRoute, Params, Router} from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-monthly',
  templateUrl: './monthly.component.html',
  styleUrls: ['./monthly.component.css']
})
export class MonthlyComponent implements OnInit {

  choosePlan:string;
  numOfMonths:number;

  constructor(private dataService: DataService,private router: Router, private route:ActivatedRoute, private location: Location) { }

  cancel() {
    this.dataService.choosePlan = '';
    this.location.back(); // <-- go back to previous location on cancel
  }

  close(){
    this.router.navigate(['./']);
  }

  ngOnInit() {

    this.choosePlan = this.dataService.choosePlan;
  }

  onClickMonthlyDetail(num){
    this.dataService.numOfMonths = num;
    this.router.navigate(['/gift/monthly/details']);
  }
}
