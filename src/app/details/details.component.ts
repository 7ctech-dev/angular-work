import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router} from '@angular/router';
import { DataService } from '../data.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  choosePlan:string ;
  numOfMonths:number;

  cookieValue = '';

  constructor(private cookieService: CookieService,private dataService: DataService,private router: Router,private location: Location) { }

  cancel() {
    this.location.back(); // <-- go back to previous location on cancel
  }

  close(){
    this.router.navigate(['./']);
  }

  ngOnInit() {
    this.cookieValue = this.cookieService.get('checkEmail');

    this.choosePlan = this.dataService.choosePlan;
    this.numOfMonths = this.dataService.numOfMonths;

  }

  onClickRegister(price){
    if(this.cookieValue){
      this.router.navigate(['./gift/dashboard']);
    }else{
    this.dataService.choosePlan = this.dataService.choosePlan
    this.dataService.numOfMonths = this.dataService.numOfMonths
    this.dataService.price = price;

    this.router.navigate(['/gift/register']);
    }
  }

}
