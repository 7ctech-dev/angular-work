import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleComponent } from './single/single.component';
import { MonthlyComponent } from './monthly/monthly.component';
import { DetailsComponent } from './details/details.component';
import { RegisterComponent } from './register/register.component';
import { GiftComponent } from './gift/gift.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
    { path:'', redirectTo:'/gift', pathMatch:'full'},
    { path:"gift",  component:GiftComponent },
    { path:"gift/single",  component:SingleComponent },
    { path:"gift/monthly",  component:MonthlyComponent },
    { path:"gift/monthly/details",  component:DetailsComponent },
    { path:"gift/register", component:RegisterComponent  },
    { path:"gift/dashboard", component:DashboardComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
