import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SingleComponent } from './single/single.component';
import { MonthlyComponent } from './monthly/monthly.component';
import { DetailsComponent } from './details/details.component';
import { RegisterComponent } from './register/register.component';
import { GiftComponent } from './gift/gift.component';
import { DataService } from './data.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import {NgxMaskModule} from 'ngx-mask'

@NgModule({
  declarations: [
    AppComponent,
    SingleComponent,
    MonthlyComponent,
    DetailsComponent,
    RegisterComponent,
    GiftComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxMaskModule.forRoot()
  ],
  providers: [DataService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
