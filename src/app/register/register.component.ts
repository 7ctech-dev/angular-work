import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {Router} from "@angular/router";
import { DataService,  User } from '../data.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';

import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  cookieValue = '';

  public name = '';

  choosePlan:string ;
  numOfMonths:number;
  price:number;
  email:string;

  phone:number;
  birthday:string;

  constructor(
     private cookieService: CookieService,
     private http:HttpClient,
     private formBuilder: FormBuilder, 
     private dataService: DataService,
     private router: Router, 
     private location: Location) { }

  ngOnInit() {

    //this.cookieService.delete('test');
    //this.cookieService.set( 'Test', 'Hello World' );
    this.cookieValue = this.cookieService.get('checkEmail');


    this.choosePlan = this.dataService.choosePlan;
    this.numOfMonths = this.dataService.numOfMonths;
    this.price = this.dataService.price;

    //const expiryRegex = '^(((0)[0-9])|((1)[0-2]))([0-2][0-9]|(3)[0-1])[0-9]{4}$';
    const expiryRegex = '^(((0)[0-9])|((1)[0-2]))([0-2][0-9]|(3)[0-1])(19[5-9][0-9]|20[0][0-9]|2010)$';

    this.form = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      lastName: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      phone:['',[Validators.required, Validators.pattern('^[0-9]+$')]],
      birthday:['',[Validators.required,Validators.maxLength(8), Validators.pattern(expiryRegex)]]
     
    });
  }

  birthday2(){
    //console.log(this.form.value.birthday)

    // this.date_of_birth =  this.form.value.birthday;

    // if(this.date_of_birth.length == 8){

    //   console.log(this.date_of_birth.substr(0, 2)+'-'+this.date_of_birth.substr(2, 2)+'-'+this.date_of_birth.substr(4, 8));

    //   if(this.date_of_birth.substr(0, 2) >= '1' || this.date_of_birth.substr(0, 2) <= '12'){
    //     console.log('Month'+this.date_of_birth.substr(0, 2));
    //     //this.dobmonth = true;
    //   }
      
    //   if(this.date_of_birth.substr(2, 2) >= '1' || this.date_of_birth.substr(2, 2) <= '31'){
    //     console.log('Day'+this.date_of_birth.substr(2,2));
    //     //this.dobday = true;
    //   }

    //   if(this.date_of_birth.substr(4, 8) >= '1950' || this.date_of_birth.substr(4, 8) <= '2010'){
    //     console.log('Year'+this.date_of_birth.substr(4, 8));
    //     //this.dobyear = true;
    //   }
    // }
    
  }  

  close(){
    this.router.navigate(['./']);
  }

  
  cancel() {
    this.choosePlan = '';
    this.location.back(); // <-- go back to previous location on cancel
  }
 
  submit() {
    if (this.form.valid) {
     // console.log(this.form.value);
     this.form.value.chooseplan = this.choosePlan;

     if(this.dataService.numOfMonths == null){
      this.form.value.numofmonths = 0; 
     }else{
      this.form.value.numofmonths = this.numOfMonths;
     }
    
     this.form.value.price = this.price;
      console.log(this.form.value);

      const httpParams = new HttpParams()
                          .set('firstname', this.form.value.firstName)
                          .set('lastname', this.form.value.lastName)
                          .set('email', this.form.value.email)
                          .set('password', this.form.value.password)
                          .set('plantype', this.form.value.chooseplan)
                          .set('monthly',  this.form.value.numofmonths)
                          .set('price', this.form.value.price)
                          .set('phone', this.form.value.phone)
                          .set('birthday', this.form.value.birthday);

      let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

      this.email = this.form.value.email;

      this.http.post("http://localhost/gift/data.php", httpParams.toString(), {
        observe: 'response',
        responseType: 'text',
        headers: headerOptions
           })
        .subscribe(
            data => {
                console.log("POST Request is successful ", data);
                this.cookieService.set( 'checkEmail',  this.email );
                this.cookieValue = this.cookieService.get('checkEmail');
                this.router.navigate(['./gift/dashboard']);
            },
            error => {
                console.log("Error", error);
            }
        );   

      this.form.reset();
    }
  }

}