import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from '../data.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  choosePlan:string;
  users:any;
  cookieValue = '';

  constructor(private cookieService: CookieService, private data:DataService,private router: Router,private location:Location) { }

  ngOnInit() {
    
    this.data.getData('http://localhost').subscribe(data=>{
      console.log(data);
      this.users = data;
    });

      if(this.cookieService.get('checkEmail')){
        this.cookieValue = this.cookieService.get('checkEmail');
      }
  }

  close(){
    this.router.navigate(['./']);
  }

  
  cancel() {
    this.choosePlan = '';
    this.location.back(); // <-- go back to previous location on cancel
  }

}
